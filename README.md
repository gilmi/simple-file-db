# simple-file-db

A very very simple way to save data in a file and interact with it in a concurrent setting.

Note that this is not tolerant of async IO exceptions.
Use this for your toy projects if you'd like but don't trust it!
