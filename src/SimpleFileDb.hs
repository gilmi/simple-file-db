-- | Interact with the db file

module SimpleFileDb
  ( DBVar
  , DB
  , DBMessage(..)
  , initializeDB
  , dbWorker
  , readDB
  , writeDB
  , modifyDB
  , readDBIO
  , writeDBIO
  , modifyDBIO
  )
where

import Relude

import Data.Functor
import System.Directory
import System.FilePath
import Control.Concurrent.STM (TBQueue, readTBQueue, writeTBQueue, newTBQueueIO)
import Control.Concurrent (forkIO)
import Data.Serialize

data DBMessage db
  = Save db

data DBVar db
  = DBVar
  { getDbVar :: TVar db
  , getMsgQueue :: TBQueue (DBMessage db)
  }

type DB db = (Serialize db)

-- Actions --

initializeDB
  :: DB db => MonadIO m => FilePath -> db -> m (DBVar db)
initializeDB dbfile emptydb = do
  liftIO $ createDirectoryIfMissing True (takeDirectory dbfile)
  dbFileExists <- liftIO $ doesFileExist dbfile
  if dbFileExists
    then do
      runReaderT loadStateIO dbfile >>= \case
        Left err ->
          error $ "Failed to load file.\n  - " <> toText err
        Right db -> do
          mkDBVar dbfile db
    else do
      runReaderT (saveStateIO emptydb) dbfile
      mkDBVar dbfile emptydb

mkDBVar :: DB db => MonadIO m => FilePath -> db -> m (DBVar db)
mkDBVar dbfile db = do
  dbv <- liftIO $ newTVarIO db
  dbq <- liftIO $ newTBQueueIO 100
  let dbvar = DBVar dbv dbq
  liftIO $ forkIO $ dbWorker dbfile dbvar
  pure dbvar

getDbFilePath :: MonadReader FilePath m => m FilePath
getDbFilePath = ask

saveStateIO :: DB db => MonadIO m => MonadReader FilePath m => db -> m ()
saveStateIO x = do
  dbfile <- getDbFilePath
  writeFileBS dbfile $ encode x

loadStateIO :: MonadIO m => DB db => MonadReader FilePath m => m (Either String db)
loadStateIO = do
  dbfile <- getDbFilePath
  decode <$> readFileBS dbfile

dbWorker :: DB db => FilePath -> DBVar db -> IO ()
dbWorker cfg dbvar =
  forever $ do
    atomically (readTBQueue (getMsgQueue dbvar)) >>= \case
      Save db ->
        runReaderT (saveStateIO db) cfg

readDB :: DBVar db -> STM db
readDB dbVar = readTVar $ getDbVar dbVar

writeDB :: db -> DBVar db -> STM ()
writeDB db dbvar = do
  writeTVar (getDbVar dbvar) db
  writeTBQueue (getMsgQueue dbvar) (Save db)

modifyDB :: DB db => DBVar db -> (db -> db) -> STM ()
modifyDB dbvar f = flip writeDB dbvar . f =<< readDB dbvar

readDBIO :: DB db => MonadIO m => DBVar db -> m db
readDBIO = atomically . readDB

writeDBIO :: DB db => MonadIO m => db -> DBVar db -> m ()
writeDBIO db = atomically . writeDB db

modifyDBIO :: DB db => MonadIO m => DBVar db -> (db -> db) -> m ()
modifyDBIO mystate f = atomically $ modifyDB mystate f

